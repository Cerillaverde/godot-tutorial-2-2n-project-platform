extends CanvasLayer

@onready var player : CharacterBody2D = get_tree().get_nodes_in_group("Player")[0]

func _ready():
	get_node("TextureProgressBar").max_value = player.health
	
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if is_instance_valid(player):
		get_node("TextureProgressBar").value = player.health
		$HBoxContainer/Label.text = str(GLOBAL.coins)
