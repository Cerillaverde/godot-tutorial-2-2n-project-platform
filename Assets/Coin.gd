extends Area2D

@export_enum("Coin", "Chest") var type : String = "Coin"
@onready var active : bool = true


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	$AnimationPlayer.play("Idle")


func _on_body_entered(body):
	if body.is_in_group("Player"):
		$AnimationPlayer.play("Join")


func _on_animation_player_animation_started(anim_name):
	match anim_name:
		"Join":
			if active:
				active = false
				
				match type:
					"Coin":
						GLOBAL.coins += 1
					"Chest":
						GLOBAL.coins += 100


func _on_animation_player_animation_finished(anim_name):
	match anim_name:
		"Join":
			queue_free()
