extends CharacterBody2D
class_name BasicEnemy

@onready var camera : Camera2D = get_tree().get_nodes_in_group("Camera")[0]

const FLOOR = Vector2(0, -1)
const GRAVITY = 16

@export var health : int = 3

#@onready var velocity : Vector2 = Vector2.ZERO
@onready var can_move : bool = true
@onready var direction : int = 1
const MIN_SPEED = 32
const MAX_SPEED = 64
var speed : int


func _ready() -> void:
	$AnimationPlayer.play("Star_Run")

func _process(_delta) -> void:
	patrol_ctrl()
	attack_ctrl()
	if can_move:
		motion_ctrl()
#	if health <= 0:
#		queue_free()

func motion_ctrl() -> void:
	if direction == 1:
		$Sprite2D.flip_h = true
	else:
		$Sprite2D.flip_h = false
	
	if is_on_wall() or not $Raycast/Ground.is_colliding():
		direction *= -1
		$Raycast.scale.x *= -1
	
	velocity.y *= GRAVITY
	velocity.x = speed * direction
	
	move_and_slide()

func damage_ctrl(damage) -> void:
	if can_move:
		if health > 0:
			health -= damage
			$AnimationPlayer.play("Star_Hit")
			print("La vida del enemigo es igual a: "+str(health))
		else:
			$AnimationPlayer.play("Star_DeadHit")

func patrol_ctrl() -> void:
	if get_node("Raycast/Patrol").is_colliding():
		if get_node("Raycast/Patrol").get_collider().is_in_group("Player"):
			speed = MAX_SPEED
		else:
			speed = MIN_SPEED
	else:
		speed = MIN_SPEED
	

func attack_ctrl() -> void:
	if get_node("Raycast/Attack").is_colliding():
		if get_node("Raycast/Attack").get_collider().is_in_group("Player"):
			can_move = false
			get_node("AnimationPlayer").play("Attack")


func _on_animation_player_animation_started(anim_name):
	match anim_name:
		"Star_Hit":
			can_move = false
			camera.screen_shake(0.5, 0.6, 100)
		"Attack":
			get_node("Raycast/Attack").get_collider().damage_ctrl(1)


func _on_animation_player_animation_finished(anim_name):
	match anim_name:
		"Star_Hit":
			if health > 0:
				can_move = true
				$AnimationPlayer.play("Star_Run")
			else:
				$AnimationPlayer.play("Star_DeadHit")
		"Attack":
			can_move = true
			get_node("AnimationPlayer").play("Star_Run")
		"Star_DeadHit":
			queue_free()
