extends CharacterBody2D

const SPEED = 128
const FLOOR = Vector2(0, -1)
const GRAVITY = 10
const JUMP_HEIGHT = 500
const BOUNCING_JUMP = 250
const CAST_WALL = 8
const CAST_ENEMY = 22
@onready var motion = Vector2.ZERO
var can_move : bool
var immunity : bool = false
var health : int = 5

""" STATE MACHINE """
var playback : AnimationNodeStateMachinePlayback

func _ready():
	playback = $AnimationTree.get("parameters/playback")
	playback.start("Idle")
	$AnimationTree.active = true

func _process(_delta):
	motion_ctrl()
#	direction_ctrl()
	jump_ctrl()
	attack_ctrl()


func motion_ctrl():
	velocity.y += GRAVITY
	
	if can_move:
		velocity.x = GLOBAL.get_axis().x * SPEED

		if GLOBAL.get_axis().x == 0:
			playback.travel("Idle")
		elif GLOBAL.get_axis().x == 1:
			playback.travel("Run")
			$Sprite.flip_h = false
		elif GLOBAL.get_axis().x == -1:
			playback.travel("Run")
			$Sprite.flip_h = true
		
		match playback.get_current_node():
			"Idle":
				velocity.x = 0
				$CPUParticles2D.emitting = false
			"Run":
				$CPUParticles2D.emitting = true

	match $Sprite.flip_h:
		true:
			$Raycast.scale.x = -1
		false:
			$Raycast.scale.x = 1
		
	move_and_slide()
	
	var slide_count =  get_slide_collision_count()
	
	if slide_count:
		var collision = get_slide_collision(slide_count - 1)
		var collider = collision.get_collider()
		
		if collider.is_in_group("Platform") and Input.is_action_just_pressed("ui_down"):
			$Collision.disabled = true
			$Timer.start()
	

func _on_timer_timeout():
	$Collision.disabled = false


func jump_ctrl():
	match is_on_floor():
		true:
			can_move = true
			$Raycast/Wall.enabled = false
			
			if Input.is_action_pressed("jump"):
				velocity.y -= JUMP_HEIGHT
		
		false:
			$CPUParticles2D.emitting = false
			$Raycast/Wall.enabled = true
			
			if velocity.y < 0:
				playback.travel("Jump")
			else:
				playback.travel("Fall")
			
			if $Raycast/Wall.is_colliding():
				
				var body = $Raycast/Wall.get_collider()
				
				if body.is_in_group("Wall") and Input.is_action_just_pressed("jump"):
					can_move = false
					velocity.y -= JUMP_HEIGHT
					
					if $Sprite.flip_h:
						velocity.x += BOUNCING_JUMP
						$Sprite.flip_h = false
					else:
						velocity.x -= BOUNCING_JUMP
						$Sprite.flip_h = true
	
func attack_ctrl():
	var body = $Raycast/Enemy.get_collider()
	
	if is_on_floor():
		if GLOBAL.get_axis().x == 0 and Input.is_action_just_pressed("attack"):
			match playback.get_current_node():
				"Idle":
					playback.travel("Attack-1")
				"Attack-1":
					playback.travel("Attack-2")
				"Attack-2":
					playback.travel("Attack-3")
			
			if $Raycast/Enemy.is_colliding():
				if body.is_in_group("Enemy"):
					body.damage_ctrl(3)
	

func damage_ctrl(damage : int) -> void:
	match immunity:
		false:
			health -= damage
			get_node("AnimationPlayer").play("Hit")
			immunity = true



func _on_animation_player_animation_finished(anim_name):
	match anim_name:
		"Hit":
			immunity = false
