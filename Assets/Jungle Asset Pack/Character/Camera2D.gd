extends Camera2D

@onready var player = get_tree().get_nodes_in_group("Player")[0]
@onready var rng = RandomNumberGenerator.new()

func random(min_number, max_number):
	rng.randomize()
	var random = rng.randf_range(min_number, max_number)
	return random

func _process(_delta):
	global_position.x = player.global_position.x

func shake_camera(shake_power : float) -> void:
	self.position.x = random(self.position.x - shake_power, self.position.x + shake_power)
	self.position.y = random(self.position.y - shake_power, self.position.y + shake_power)

func screen_shake(shake_lenght : float, shake_power : float, shake_priority : int) -> void:
	var current_shake_priority : int = 0
	var tween : Tween = get_tree().create_tween().set_trans(Tween.TRANS_SINE).set_ease(Tween.EASE_OUT)
	var origin = self.position
	if shake_priority > current_shake_priority:
		tween.tween_method(shake_camera, shake_power, 0, shake_lenght)
	self.position = origin
