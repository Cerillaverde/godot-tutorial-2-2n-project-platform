extends Area2D

@export var Shoot : PackedScene

@onready var player : CharacterBody2D = get_tree().get_nodes_in_group("Player")[0]
var velocity : float
var can_shoot : bool = true
#var tween : Tween = get_tree().create_tween().set_trans(Tween.TRANS_SINE).set_ease(Tween.EASE_OUT)

func _ready():
	$AnimatedSprite2D.play("Idle")


func _process(_delta) -> void:
	
	if is_instance_valid(player):
		motion_ctrl()
		tween_ctrl()
	
	if Input.is_action_just_pressed("shoot") and can_shoot:
		shoot_ctrl()



func motion_ctrl():
	if player.get_node("Sprite").flip_h:
		scale.x = -1
		velocity = player.global_position.x + 22
	else:
		scale.x = 1
		velocity =  player.global_position.x - 22

func tween_ctrl() -> void:
	
	var tween : Tween = get_tree().create_tween().set_trans(Tween.TRANS_SINE).set_ease(Tween.EASE_OUT)
	tween.tween_property(
		self,#objeto afectado
		"global_position", #propiedad afectada
		Vector2(velocity, player.global_position.y - 8),#valor final
		0.4,#tiempo de transicion
	)
#	tween.start()

func shoot_ctrl():
	can_shoot = false
	var shoot = Shoot.instantiate()
	shoot.global_position = $ShootSpawn.global_position
	
	if player.get_node("Sprite").flip_h:
		shoot.scale.x = -1
		shoot.direction = -224
	else:
		shoot.scale.x = 1
		shoot.direction = 224
	
	get_tree().call_group("Level", "add_child", shoot)
	
	cooldown()
	

func cooldown():
	await get_tree().create_timer(1.0).timeout
	can_shoot = true
