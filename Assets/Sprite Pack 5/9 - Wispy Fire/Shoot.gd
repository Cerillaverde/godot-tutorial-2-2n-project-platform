extends Area2D

var direction : int
@onready var can_move : bool = true

# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimationPlayer.play("Shoot")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if can_move:
		global_position.x += direction * delta


func _on_visible_on_screen_notifier_2d_screen_exited():
	queue_free()


func _on_body_entered(body):
	if body.is_in_group("Enemy"):
		body.damage_ctrl(1)
		$AnimationPlayer.play("Explosion")
	elif body.is_in_group("Wall"):
		$AnimationPlayer.play("Explosion")


func _on_animation_player_animation_started(anim_name):
	match anim_name:
		"Explosion":
			can_move = false


func _on_animation_player_animation_finished(anim_name):
	match anim_name:
		"Explosion":
			queue_free()
